import React from 'react'
import OrderTrackingMenu from '../../Basic/OrderTrackingMenu'
import LiveTrackingSection from '../../Basic/LiveTrackingSection'
import './orderTrackingSection.css'

const OrderTrackingSection =(props)=>{
  return(
    < div className = "orderTrackingSection-container" >
      <OrderTrackingMenu />
      <LiveTrackingSection 
      onClick={props.onClick}
      mapClick={props.mapClick}
      detailsClick={props.detailsClick}
      />
    </div>
  )
}

export default OrderTrackingSection
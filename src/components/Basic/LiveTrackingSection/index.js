import React from 'react'
import './liveTrackingSection.css'
import infoIcon from '../../../images/info-icon.svg'
import LiveTrackingInfo from '../../Basic/LiveTrackingInfo'
import menuIcon from '../../../images/menu-button.svg'

const LiveTrackingSection =(props)=>{
  return(
    <div className="liveTrackignSection-container">
      <div className="liveTrackingSection-menu-button" onClick={props.onClick} >
        <img className="menu-button" src={menuIcon} alt="menu icon"/>
      </div>

      <div className="liveTrackignSection-title-container">
        <span>Seguimiento en vivo</span>
        <img className="liveTrackignSection-info-icon" src={infoIcon} alt="information icon"/>
      </div>
      <LiveTrackingInfo 
      mapClick={props.mapClick}
      detailsClick={props.detailsClick}
      />
    </div>
  )
}

export default LiveTrackingSection
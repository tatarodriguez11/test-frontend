import React, {useState} from 'react'
import './orderInfo.css'
import greenSpinner from '../../../images/loading-green.svg'
import showInfo from '../../../images/show-info.png'
import locationIconColor from '../../../images/location-a-color.png'
import locationB from '../../../images/location-b.png'
import locationC from '../../../images/location-c.png'
import {useSpring, animated} from 'react-spring'

const OrderInfo =()=>{

  const [show, toggle] = useState(false)
  const showStyle = useSpring({
    opacity: show ? 1 : 0,
    transform : show ? 'translateY(0px)' : 'translateY(-90px)'
  })

  const imageStyle = useSpring({
    transform : show ? 'rotate(180deg)' : 'rotate(0deg)'
  })

  return(
    
    <div  className="orderInfo-container">

      <div className="orderInfo-main-info">
        <div className="orderInfo-state-container">
            <img className="orderInfo-loading-spinner"  src={greenSpinner} alt="loading green spinner"/>
            <div className="orderInfo-state-description-container">
                <p className="orderInfo-state">Estado</p>
                <p className="orderInfo-state-description">Tu Rappi esta en camino a la dirección indicada</p>
            </div>
            <animated.img className="orderInfo-show-button" onClick={()=>toggle(!show)} style={imageStyle} src={showInfo} alt="show info green button"/>
        </div>
        <div className="orderInfo-transport-info-container">
          <div className="orderInfo-rappi-profile-container">
          < img 
          className="orderInfo-rappi-profile-pic"
          src = "https://starsunfolded.com/wp-content/uploads/2016/06/Kendall-Jenner-1.jpg"
          alt = "" />
          </div>
          <div className="orderInfo-transport-info">
              <p className="orderInfo-rappi-info">Tu Rappi: Kendall N. Jenner</p>
              <p className="orderInfo-contact">300 245 7890     -     Placa: EXT 382</p>
          </div>
        </div>
      </div>

      <animated.div className="orderInfo-divider" style={showStyle}>


          <div className="orderInfo-open-address">          
            <div  className="open-address-main-data">
                <img className="open-address-location-icon" src={locationIconColor} alt="location icon red colored"/>
                <div className="orderInfo-transport-info">
                    <p className="orderInfo-first-address"> <span style={{fontWeight:900}}>Calzado Med </span> - Calle 151 # 109a - 25</p>
                    <p className="orderInfo-first-address-detail">Oficina 507     -     301 603 3252</p>
                </div>
            </div>
                <p className="orderInfo-open-address-detail">Pulvinar purus. Suspendisse congue erat vel purus fermentum sodales elit, sed consequat lorem dui a est, donec id rutrum ante. Curabitur luctus arcu eget varius aliquam.</p>
          </div>
      
      

          <div className="orderInfo-close-address">
            <div className="orderInfo-address-container">
                <img className="open-address-location-icon" src={locationB} alt="location icon red colored"/>
                <div className="orderInfo-transport-info">
                    <p className="orderInfo-first-address"> Calle 151 # 109a - 25</p>
                    <p className="orderInfo-first-address-detail">Recoger 200 bolsas de alimento par…</p>
                </div>
            </div>
          </div>

          <div className="orderInfo-close-address">
            <div className="orderInfo-address-container">
                <img className="open-address-location-icon" src={locationC} alt="location icon red colored"/>
                <div className="orderInfo-transport-info">
                    <p className="orderInfo-first-address">Calle 151 # 109a - 25</p>
                    <p className="orderInfo-first-address-detail">Recoger 200 bolsas de alimento par…</p>
                </div>
            </div>
          </div>

          <div className="orderInfo-id-price-container">
            <div className="orderInfo-id-container">
              <span >ID</span>
              <span>0003457221</span>
            </div>
            <div className="orderInfo-price-container">
              <span >Costo total</span>
              <span>$350.000</span>
            </div>
          </div>
      </animated.div>
      
      
    </div>
        
  )
}

export default OrderInfo
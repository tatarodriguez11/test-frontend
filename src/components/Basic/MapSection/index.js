import React from 'react'
import './mapSection.css'
import mapImage from '../../../images/map1.png'

const MapSection =(props)=>{
  return(
    <div className="mapSection" style={{backgroundImage:`url(${mapImage})`}}>
      {props.children}
    </div>
  )
}

export default MapSection
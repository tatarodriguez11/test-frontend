import React from 'react'
import './mapImage.css'
import map from '../../../images/map1.png'

const MapImage =()=>{
  return(
    <div
    className="mapImage"
    style={{backgroundImage:`url(${map})`}}
    ></div>
  )
}

export default MapImage
import React from 'react'
import './orderTrackingMenu.css'

const OrderTrackingMenu =()=>{

  return(
    <div className="orderTracking-container">
      <ul className="orderTracking-content">
        <li className="orderTracking-item" >Crear orden</li>
        < li className = "orderTracking-item orderTracking-item-active" > Seguimiento </li>
        <li className="orderTracking-item">Programadas</li>
      </ul>
    </div>
  )
}


export default OrderTrackingMenu

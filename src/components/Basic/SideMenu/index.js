import React, {Fragment} from 'react'
import './sideMenu.css'
import logo from '../../../images/bigote.svg'
import logout from '../../../images/logout.svg'
import {useSpring, animated} from 'react-spring'

const SideMenu = (props)=>{
  const show = props.show ? "show-sidebar" : "no-show-sidebar"
  const overlay = props.show ? "overlay" : "no-overlay"

  const style = useSpring({
    opacity: props.show ? '1' : '0'

  })

  const barStyle = useSpring({
    // display : props.show ? 'block' : 'none',
    width: props.show ? "200px" : "0px"

  })

  const imgStyle = useSpring({
    transform: props.show ? 'scale(1)': 'scale(0)'
  })

  return(
    <Fragment>
      < animated.div 
      className = {'sidebar ' + show}  
      style={barStyle}
      >
        < div className = "sidebar-image" >
          <animated.img 
          style={imgStyle}
          src = {logo}
          alt="rappi-logo"
          className="sidebar-logo"
          onClick={props.onClick}
          />
        </div>
        <ul className="sidebar-content">
          <li className="item-side sidebar-item1 active" href="#home">Inicio rápido</li>
          < li className = "item-side sidebar-item2" > Centro de ayuda </li>
          < li className = "item-side sidebar-item3" > Histórico de envíos </li>
          < li className = "item-side sidebar-item4" > Configuración </li>
        </ul>
        <div className="sidebar-logout">
          <img className="sidebar-logout-icon" src={logout} alt=""/>
          <span>Cerrar sesión</span> 
        </div>
      </animated.div>
      <animated.div 
      className={overlay}
      style={style}
      onClick={props.onClick}
      ></animated.div>
    </Fragment>
  )
}

export default SideMenu
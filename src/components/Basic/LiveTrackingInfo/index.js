import React from 'react'
import './liveTrackingInfo.css'
import loadingSpinner from '../../../images/loading.svg'
import locationA from '../../../images/location-a.png'
import locationB from '../../../images/location-b.png'


const LiveTrackingInfo =(props)=>{
  return(
    <div className="liveTrackingInfo-container">
      <div className="liveTrackingInfo-id-container">
        <span className="liveTrackingInfo-id">ID 00037509</span>
        <div className="liveTrackingInfo-searching-container">
          <img className="liveTrackingInfo-loading-spinner" src={loadingSpinner} alt="loading spinner"/>
          <p className="liveTrackingInfo-searching-text"> Buscando RT...</p>
        </div>
      </div>
      <div className="liveTrackingInfo-address-container">
        <img src={locationA} alt="location icon" className="liveTrackingInfo-location-icon" />
        <div className="liveTrackingInfo-address-order-container">
          <p className="liveTrackingInfo-address">
            <span style={{fontWeight:900}}> Calzado Med </span> - Calle 151 # 109a - 25 T2…
          </p>        
          <p className="liveTrackingInfo-order">
            Recoger 200 bolsas de alimento para animales (perros y gatos)
          </p>
        </div>
      </div>
      <div className="liveTrackingInfo-address-container">
        <img src={locationB} alt="location icon" className="liveTrackingInfo-location-icon" />
        <div className="liveTrackingInfo-address-order-container">
          <p className="liveTrackingInfo-address">
            Calle 151# 109 a - 25
          </p>        
          <p className="liveTrackingInfo-order">
            Entregar 50 bolsas en este punto. 10 son de alimento de perro y las otras de alimento de gato
          </p>
        </div>
      </div>
      <div className="liveTrackingInfo-buttons-container">
        <button onClick={props.mapClick} className="liveTrackingInfo-map-button">Ver mapa</button>
        <button onClick={props.detailsClick} className="liveTrackingInfo-details-button">Ver detalles</button>
      </div>
    </div>
  )
}

export default LiveTrackingInfo
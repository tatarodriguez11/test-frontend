import React from 'react'
import './modal.css'
import {useSpring, animated} from 'react-spring'
import closeIcon from '../../../images/close-icon.svg'

const Modal = ({show,children, onClick})=>{
  const showHideModal = show ? 'open modal' : 'close'

  const sectionStyles=useSpring({
    opacity: show ? 1 : 0
  })

  return(
    <div className={showHideModal} >
      <animated.section className="container" style={sectionStyles}>
      <button className="close-modal"onClick={onClick}>
        <img  className="close-modal-icon" src={closeIcon} alt="close icon"/>
      </button>
        {children}
      </animated.section>
    </div>
  )
}

export default Modal
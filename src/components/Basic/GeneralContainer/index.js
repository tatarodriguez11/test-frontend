import React from 'react'
import './generalContainer.css'


const GeneralContainer=(props)=>{
  return(
    <div className="general-container" onClick={props.onClick}>
      {props.children}
    </div>
  )
}

export default GeneralContainer
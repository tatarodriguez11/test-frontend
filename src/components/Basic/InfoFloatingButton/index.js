import React from 'react'
import './infoFloatingButton.css'
import infoIcon from '../../../images/ico-chat.svg'

const InfoFloatingButton = ()=>{


  return(
    <div className="infoFloatingButton-container" >
      <img src={infoIcon} alt=""/>
    </div>
  )
}

export default InfoFloatingButton
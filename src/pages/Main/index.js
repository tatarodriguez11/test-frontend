import React from 'react'
import Wrapper from '../../components/Basic/Wrapper'
import SideMenu from '../../components/Basic/SideMenu'
import GeneralContainer from '../../components/Basic/GeneralContainer'
import OrderTrackingSection from '../../components/Compound/OrderTrackingSection'
import MapSection from '../../components/Basic/MapSection'
import InfoFloatingButton from '../../components/Basic/InfoFloatingButton'
import OrderInfo from '../../components/Basic/OrderInfo'
import Modal from '../../components/Basic/Modal'
import MapImage from '../../components/Basic/MapImage'


class Main extends React.Component{

  state={
    show: false,
    showModal:false,
    modalContent: null
  }

  onClick=()=>{
    const w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)

    if(w > 768)return;
    
    this.setState({
      show : !this.state.show
    })
  }  

  onResize=()=>{
    const w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
    if (w > 768) {
      if(this.state.show) return ;
      this.setState({
        show: true
      })
    }else{
      if(!this.state.show) return;
      this.setState({
        show:false
      })
    }
  }

  mapClick=()=>{

    this.setState({
      showModal:true,
      modalContent: <MapImage />
    })

  }

  detailsClick=()=>{
    this.setState({
      showModal:true,
      modalContent: <OrderInfo />
    })
  }

  closeModal=()=>{
    this.setState({
      showModal:false
    })
  }

  componentDidMount(){

    this.onResize()
    window.addEventListener('resize', this.onResize)
  }

  render(){
    return(
      <Wrapper>
        <GeneralContainer
        >
        
        <Modal
          show={this.state.showModal}
          onClick={this.closeModal}
        >
          {this.state.modalContent}
        </Modal>

        <SideMenu 
        show={this.state.show}
        onClick={this.onClick}

        />
        <OrderTrackingSection 
        onClick={this.onClick}
        mapClick={this.mapClick}
        detailsClick={this.detailsClick}
        />

        <MapSection >
        <OrderInfo />
        </MapSection>
        
        <InfoFloatingButton />
        </GeneralContainer>
      </Wrapper>
    )
  }
}

export default Main
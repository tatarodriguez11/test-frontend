import React, {Fragment} from 'react';
import {Route , Switch} from 'react-router-dom'
import Main from './pages/Main'


function App() {
  return (
   <Fragment>
     <Switch>
      <Route exact path="/" component={Main}/>
     </Switch>
   </Fragment>
  );
}

export default App;
